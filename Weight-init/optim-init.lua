--
-- Different weight initialization methods
--
-- > model = require('optim-init')(model, 'uniform', value)
--
require("nn")


-- initialize to `val`
local function optim_init_uniform(m,val)
    if m.weight then
        m.weight:fill(val)
    end
    if m.bias then
        m.bias:fill(val)
    end
end

local function optim_init(net, arg, val)
   -- choose initialization method
   local method = nil
   if     arg == 'uniform'    then method = optim_init_uniform
   --elseif arg == 'xavier'       then method = w_init_xavier
   else
      assert(false)
   end

   -- loop over all convolutional modules
   for i = 1, #net.modules do
      local m = net.modules[i]
      if m.__typename == 'nn.SpatialConvolution' then
         method(m,val)
      elseif m.__typename == 'nn.SpatialConvolutionMM' then
         method(m,val)
      elseif m.__typename == 'cudnn.SpatialConvolution' then
         method(m,val)
      elseif m.__typename == 'nn.LateralConvolution' then
         method(m,val)
      elseif m.__typename == 'nn.VerticalConvolution' then
         method(m,val)
      elseif m.__typename == 'nn.HorizontalConvolution' then
         method(m,val)
      elseif m.__typename == 'nn.Linear' then
         method(m,val)
      elseif m.__typename == 'nn.TemporalConvolution' then
         method(m,val)
      end

   end
   return net
end


return optim_init
